package dao;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

import connector.MongoDBConnector;
import entity.Feed;
import entity.MetaData;
import entity.ResponseStatus;

public class FeedDAO {
	private MongoCollection<Document> feed_collection;
	private String collection_name = "Feed";
	
	public FeedDAO() 
	{
		feed_collection = MongoDBConnector.sharedInstance().getCollection(collection_name);
	}
	
	public Feed insertNewFeed(Feed feed)
	{
		Document feed_document = feed.toDocument();
		this.feed_collection.insertOne(feed_document);
		MetaData meta_data = new MetaData(ResponseStatus.SUCCESS);
		feed.setMeta_data(meta_data);
		return feed;
	}
}
