package dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.model.Updates;

import connector.MongoDBConnector;
import entity.MetaData;
import entity.ResponseStatus;
import entity.User;
import utils.AppUtils;

public class UserDAO 
{
	private MongoCollection<Document> user_collection;
	private String collection_name = "User";
	
	public UserDAO()
	{
		user_collection = MongoDBConnector.sharedInstance().getCollection(collection_name);
	}
	
	public User insertNewUser(User user)
	{
		User response = new User();
		Document user_document = user.toDocument();
		if (userIsExist(user))
		{
			MetaData meta_data = new MetaData(ResponseStatus.USER_ISEXIST);
			response.setMeta_data(meta_data);
		}
		else
		{
			this.user_collection.insertOne(user_document);
			response = new User(user);
			MetaData meta_data = new MetaData(ResponseStatus.SUCCESS);
			response.setMeta_data(meta_data);
		}
		return response;
	}
	
	public User insertOrUpdateNewUser(User user)
	{
		User response = new User();
		Document filter = new Document("facebook_id", user.getFacebook_id());
		FindIterable<Document> result = this.user_collection.find(filter);
		if (result.iterator().hasNext())
		{
			String token = AppUtils.getToken(user.getEmail(), user.getFacebook_id());
			Document update_response = this.user_collection.findOneAndUpdate(filter, Updates.combine(Updates.set("email", user.getEmail()), Updates.set("avatar", user.getAvatar()), 
					Updates.set("full_name", user.getFull_name()), Updates.addToSet("device_token", user.getDevice_token()), Updates.addToSet("token", token)));
			response.parseDocument(update_response);
			response.setToken(token);
			MetaData meta_data = new MetaData(ResponseStatus.SUCCESS);
			response.setMeta_data(meta_data);
		}
		else
		{
			String token = AppUtils.getToken(user.getEmail(), user.getFacebook_id());
			List<String> tokens = new ArrayList<>();
			tokens.add(token);
			Document user_document = user.toDocument();
			user_document.replace("token", tokens);
			this.user_collection.insertOne(user_document);
			response = new User(user);
			response.setToken(token);
			MetaData meta_data = new MetaData(ResponseStatus.SUCCESS);
			response.setMeta_data(meta_data);
		}
		return response;
	}
	public void userLogout(String user_id, String token)
	{
		this.user_collection.updateOne(eq("user_id",user_id), Updates.pull("token", token));
	}
	
	public User getUserWith(String email, String password, String device_token)
	{
		User response = new User();
		Document filter = new Document("email", email);
		filter.append("password", password);
		FindIterable<Document> result = this.user_collection.find(filter);
		if (result.iterator().hasNext())
		{
			Document userResult = result.iterator().next();
			response.parseDocument(userResult);
			String token = AppUtils.getToken(email, password);
			this.insertNewToken(token, device_token, email);
			response.setToken(token);
			MetaData metadata = new MetaData(ResponseStatus.SUCCESS);
			response.setMeta_data(metadata);
		}
		else
		{
			MetaData meta_data = new MetaData(ResponseStatus.WRONG_EMAIL_OR_PASSWORD);
			response.setMeta_data(meta_data);
		}
		return response;
	}
	public User getUser(User user)
	{
		User response = new User();
		ResponseStatus auth = authorizedUser(user);
		if (auth.getCode() != ResponseStatus.SUCCESS.getCode())
		{
			MetaData metadata = new MetaData(auth);
			response.setMeta_data(metadata);
			
		}
		else
		{
			Document filter = new Document("user_id", user.getUser_id());
			FindIterable<Document> result = this.user_collection.find(filter);
			if (result.iterator().hasNext())
			{
				Document userResult = result.iterator().next();
				response.parseDocument(userResult);
				MetaData metadata = new MetaData(auth);
				response.setMeta_data(metadata);
			}
			else
			{
				MetaData metadata = new MetaData(ResponseStatus.NOTEXIST);
				metadata.setMessage("Người dùng không tồn tại");
				response.setMeta_data(metadata);
			}
		}
		return response;
	}
	
	public ResponseStatus authorizedUser(User user)
	{
		FindIterable<Document> result = this.user_collection.find(eq("user_id",user.getUser_id()));
		if (result.iterator().hasNext())
		{
			Document user_document = result.iterator().next();
			List<String> tokens = user_document.get("token", ArrayList.class);
			if (tokens.contains(user.getToken()))
			{
				return ResponseStatus.TOKEN_AUTHORIZED;
			}
		}
		return ResponseStatus.UNAUTHORIZED;
	}
	
	public User updateUserInfo(User user)
	{
		User response = new User();
		Document document = this.user_collection.findOneAndUpdate(eq("user_id",user.getUser_id()), user.toDocumentUpdate());
		if (document != null)
		{
			response.parseDocument(document);
			if (user.getFull_name().length() > 0)
			{
				response.setFull_name(user.getFull_name());
			}
			if (user.getAvatar().length() > 0)
			{
				response.setAvatar(user.getAvatar());
			}
			if (user.getPoint() > 0)
			{
				response.setPoint(user.getPoint()+response.getPoint());
			}
			response.setToken(user.getToken());
			MetaData meta_data = new MetaData(ResponseStatus.SUCCESS);
			meta_data.setMessage("Cập nhật thông tin thành công");
			response.setMeta_data(meta_data);
		}
		else
		{
			MetaData meta_data = new MetaData(ResponseStatus.SOMETHING_WRONG);
			meta_data.setMessage("Có lỗi xảy ra");
			response.setMeta_data(meta_data);
		}
		return response;
	}
	public boolean checkPassword(String user_id, String oldPassword)
	{
		FindIterable<Document> response = this.user_collection.find(new Document("user_id", user_id));
		if (response.iterator().hasNext())
		{
			Document user = response.iterator().next();
			if (user.getString("password").equals(oldPassword))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	public ResponseStatus changePassword(String user_id, String newPassword)
	{
		Document response = this.user_collection.findOneAndUpdate(eq("user_id",user_id), Updates.set("password", newPassword));
		if (response != null)
		{
			return ResponseStatus.SUCCESS;
		}
		else
		{
			return ResponseStatus.SOMETHING_WRONG;
		}
	}
	public boolean userIsExist(User user)
	{
		Document filter = new Document("email", user.getEmail().toLowerCase());
		FindIterable<Document> result = this.user_collection.find(filter);
		if (result.iterator().hasNext() && result.iterator().next().getString("email").equalsIgnoreCase(user.getEmail()))
		{
			return true;
		}
		return false;
	}
	private void insertNewToken(String token, String device_token, String email)
	{
		this.user_collection.updateOne(eq("email",email), Updates.combine(Updates.addToSet("token", token), Updates.addToSet("device_token", device_token)));
	}
}
