package dao;
import entity.*;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.client.MongoCollection;

import connector.*;
public class ConfigDAO 
{
	private MongoCollection<Document> configs_collection;
	private String collection_name = "Configs";
	public ConfigDAO() 
	{
		configs_collection = MongoDBConnector.sharedInstance().getCollection(collection_name);
	}
	
	public Config getConfig(String version)
	{
		Config config = new Config();
		Document filter = new Document("current_version", version);
		Block<Document> response_block = new Block<Document>() {
			@Override
			public void apply(Document arg0) 
			{
				config.parseDocument(arg0);
			}
			
		};
		configs_collection.find(filter).forEach(response_block);
		return config;
	}
}
