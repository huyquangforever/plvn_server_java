package api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.JSONObject;

import controller.ConfigController;
import controller.FeedController;
import controller.LoginController;
import controller.RegisterController;
import controller.UserSettingController;

@Path("/v1")
public class API 
{
	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON)
	public Response testAPI()
	{
		JSONObject json = new JSONObject();
		json.put("test", "success");
		return Response.status(200).entity(json.toString()).build();
	}
	
	@GET
	@Path("/configs")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfigs(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return ConfigController.getConfig(request_body);
	}
	
	@PUT
	@Path("/register")
	@Produces(MediaType.APPLICATION_JSON)
	public Response userRegister(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return RegisterController.registerUser(request_body, request_header);
	}
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response userLogin(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return LoginController.userLogin(request_body);
	}
	
	@POST
	@Path("/facebooklogin")
	@Produces(MediaType.APPLICATION_JSON)
	public Response facebookLogin(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return LoginController.facebookLogin(request_body);
	}
	
	@POST
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response userLogout(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return LoginController.userLogout(request_body, request_header);
	}
	
	@PUT
	@Path("/feeds")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewFeed(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return FeedController.addNewFeed(request_body, request_header);
	}
	
	@POST
	@Path("/users/{user_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUserInfo(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return UserSettingController.updateUserInfo(request_body, request_header);
	}
	
	@POST
	@Path("/users/{user_id}/change_password")
	@Produces(MediaType.APPLICATION_JSON)
	public Response changePassword(@Context UriInfo request_body, @Context HttpHeaders request_header)
	{
		return UserSettingController.changePassword(request_body, request_header);
	}
}
