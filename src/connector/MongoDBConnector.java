package connector;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBConnector 
{
	private String username = "quangnh";
	private char[] password = "123456".toCharArray();
	private String dbName = "database_pcpl";
	private MongoDatabase database;
	private MongoClient mongoClient;
	
	private static MongoDBConnector instance = null;
	public static MongoDBConnector sharedInstance()
	{
		if (instance == null)
		{
			instance = new MongoDBConnector();
		}
		return instance;
	}
	
	private MongoDBConnector()
	{
		try
		{
			MongoCredential credential = MongoCredential.createCredential(username, dbName, password);
			List<MongoCredential> credentials = new ArrayList<MongoCredential>();
			credentials.add(credential);
			mongoClient = new MongoClient(new ServerAddress("localhost", 27017), credentials);
			this.database = mongoClient.getDatabase(dbName);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public MongoCollection<Document> getCollection(String name)
	{
		return this.database.getCollection(name);
	}
}

