package helper;

import org.json.JSONObject;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.*;

import javax.net.ssl.HttpsURLConnection;

public class HTTPSHelper {
	public static JSONObject GETRequest(String url)
	{
		JSONObject json_response = new JSONObject();
		URL url_request;
		try {
			url_request = new URL(url);
			HttpsURLConnection connection = (HttpsURLConnection)url_request.openConnection();
			if (connection != null)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String input;
				while ((input = br.readLine()) != null)
				{
					System.out.println(input);
					json_response = new JSONObject(input);
				}
				br.close();
			}
			
		} catch (MalformedURLException e) 
		{
			json_response.put("error", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) 
		{
			json_response.put("error", e.getMessage());
			e.printStackTrace();
		}
		return json_response;
	}
}
