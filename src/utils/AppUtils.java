package utils;

import java.security.MessageDigest;
import java.util.Date;

public class AppUtils 
{
	private static final String secrectKey = "@#Quang_Nguyen_Huy#@";
	public static String getToken(String email, String password)
	{
		Date today = new Date();
		long miniseconds = today.getTime();
		String key = secrectKey + email + password + String.valueOf(miniseconds);
		return sha256(key);
	}
	private static String sha256(String base) 
	{
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }

	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
	
}
