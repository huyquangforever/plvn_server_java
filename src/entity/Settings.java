package entity;

import org.bson.Document;

public class Settings {
	private boolean enable_new_law_notification = false;
	private boolean enable_new_comment_notification = false;
	private boolean enable_new_system_notification = false;
	private boolean enable_new_nearme_notification = false;
	
	public Document toDocument()
	{
		Document settings = new Document();
		settings.put("enable_law_notification", this.enable_new_law_notification);
		settings.put("enable_comment_notification", this.enable_new_comment_notification);
		settings.put("enable_system_notification", this.enable_new_system_notification);
		settings.put("enable_nearme_notification", this.enable_new_nearme_notification);
		return settings;
	}
	public void parseDocument(Document document)
	{
		this.enable_new_law_notification = document.getBoolean("enable_law_notification", false);
		this.enable_new_comment_notification = document.getBoolean("enable_comment_notification", false);
		this.enable_new_system_notification = document.getBoolean("enable_system_notification", false);
		this.enable_new_nearme_notification = document.getBoolean("enable_nearme_notification", false);
	}
	public Settings(boolean enable_new_law_notification, boolean enable_new_comment_notification,
			boolean enable_new_system_notification, boolean enable_new_nearme_notification) {
		super();
		this.enable_new_law_notification = enable_new_law_notification;
		this.enable_new_comment_notification = enable_new_comment_notification;
		this.enable_new_system_notification = enable_new_system_notification;
		this.enable_new_nearme_notification = enable_new_nearme_notification;
	}
	public Settings() {
		super();
	}
	public boolean isEnable_new_law_notification() {
		return enable_new_law_notification;
	}
	public void setEnable_new_law_notification(boolean enable_new_law_notification) {
		this.enable_new_law_notification = enable_new_law_notification;
	}
	public boolean isEnable_new_comment_notification() {
		return enable_new_comment_notification;
	}
	public void setEnable_new_comment_notification(boolean enable_new_comment_notification) {
		this.enable_new_comment_notification = enable_new_comment_notification;
	}
	public boolean isEnable_new_system_notification() {
		return enable_new_system_notification;
	}
	public void setEnable_new_system_notification(boolean enable_new_system_notification) {
		this.enable_new_system_notification = enable_new_system_notification;
	}
	public boolean isEnable_new_nearme_notification() {
		return enable_new_nearme_notification;
	}
	public void setEnable_new_nearme_notification(boolean enable_new_nearme_notification) {
		this.enable_new_nearme_notification = enable_new_nearme_notification;
	}
	@Override
	public String toString() {
		return "Settings [enable_new_law_notification=" + enable_new_law_notification
				+ ", enable_new_comment_notification=" + enable_new_comment_notification
				+ ", enable_new_system_notification=" + enable_new_system_notification
				+ ", enable_new_nearme_notification=" + enable_new_nearme_notification + "]";
	}
	
	
}
