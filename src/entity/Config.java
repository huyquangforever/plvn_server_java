package entity;

import org.bson.Document;
import org.json.JSONObject;

public class Config 
{
	private MetaData meta_data;
	
	private String current_version;
	private String new_version;
	private String update_message;
	private boolean force_update;
	
	private boolean enable_admob_banner;
	private boolean enable_admob_interstitial;
	private String admob_banner_key;
	private String admob_interstitial_key;
	
	private boolean enable_facebook_banner;
	private boolean enable_facebook_native;
	private boolean enable_facebook_interstitial;
	private String facebook_banner_key;
	private String facebook_native_key;
	private String facebook_interstitial_key;
	
	public String toJSONString()
	{
		JSONObject jsonObject = new JSONObject();
		JSONObject data = new JSONObject();
		data.put("current_version", this.current_version);
		data.put("new_version", this.new_version);
		data.put("update_message", this.update_message);
		data.put("force_update", this.force_update);
		
		JSONObject admob = new JSONObject();
		admob.put("enable_banner", this.enable_admob_banner);
		admob.put("admob_banner_key", this.admob_banner_key);
		admob.put("enable_interstitial", this.enable_admob_interstitial);
		admob.put("admob_interstitial_key", this.admob_interstitial_key);
		data.put("admob_ads", admob);
		
		JSONObject facebook_ads = new JSONObject();
		facebook_ads.put("enable_banner", this.enable_facebook_banner);
		facebook_ads.put("facebook_banner_key", this.facebook_banner_key);
		facebook_ads.put("enable_interstitial", this.enable_facebook_interstitial);
		facebook_ads.put("facebook_interstitial_key", this.facebook_interstitial_key);
		facebook_ads.put("enable_native", this.enable_facebook_native);
		facebook_ads.put("facebook_native_key", this.facebook_native_key);
		data.put("facebook_ads", facebook_ads);
		
		jsonObject.put("meta_data", this.meta_data.toJSONObject());
		jsonObject.put("data", data);
		return jsonObject.toString();
	}
	
	public void parseDocument(Document document)
	{
		this.current_version = document.getString("current_version");
		this.new_version = document.getString("new_version");
		this.update_message = document.getString("update_message");
		this.force_update = document.getBoolean("force_update", false);
		
		Document admob_ads = document.get("admob_ads", Document.class);
		Document facebook_ads = document.get("facebook_ads", Document.class);
		this.enable_admob_banner = admob_ads.getBoolean("enable_admob_banner", false);
		this.admob_banner_key = admob_ads.getString("admob_banner_key");
		this.enable_admob_interstitial = admob_ads.getBoolean("enable_admob_interstitial", false);
		this.admob_interstitial_key = admob_ads.getString("admob_interstitial_key");
		
		this.enable_facebook_banner = facebook_ads.getBoolean("enable_facebook_banner", false);
		this.facebook_banner_key = facebook_ads.getString("facebook_banner_key");
		this.enable_facebook_interstitial = facebook_ads.getBoolean("enable_facebook_interstitial", false);
		this.facebook_interstitial_key = facebook_ads.getString("facebook_interstitial_key");
		this.enable_facebook_native = facebook_ads.getBoolean("enable_facebook_native", false);
		this.facebook_native_key = facebook_ads.getString("facebook_native_key");
	}
	
	public Config() {
		super();
		this.meta_data = new MetaData();
	}
	public Config(MetaData meta_data, String current_version, String new_version, String update_message,
			boolean force_update, boolean enable_admob_banner, boolean enable_admob_interstitial,
			String admob_banner_key, String admob_interstitial_key, boolean enable_facebook_banner,
			boolean enable_facebook_native, boolean enable_facebook_interstitial, String facebook_banner_key,
			String facebook_native_key, String facebook_interstitial_key) {
		super();
		this.meta_data = meta_data;
		this.current_version = current_version;
		this.new_version = new_version;
		this.update_message = update_message;
		this.force_update = force_update;
		this.enable_admob_banner = enable_admob_banner;
		this.enable_admob_interstitial = enable_admob_interstitial;
		this.admob_banner_key = admob_banner_key;
		this.admob_interstitial_key = admob_interstitial_key;
		this.enable_facebook_banner = enable_facebook_banner;
		this.enable_facebook_native = enable_facebook_native;
		this.enable_facebook_interstitial = enable_facebook_interstitial;
		this.facebook_banner_key = facebook_banner_key;
		this.facebook_native_key = facebook_native_key;
		this.facebook_interstitial_key = facebook_interstitial_key;
	}
	public MetaData getMeta_data() {
		return meta_data;
	}
	public void setMeta_data(MetaData meta_data) {
		this.meta_data = meta_data;
	}
	public String getCurrent_version() {
		return current_version;
	}
	public void setCurrent_version(String current_version) {
		this.current_version = current_version;
	}
	public String getNew_version() {
		return new_version;
	}
	public void setNew_version(String new_version) {
		this.new_version = new_version;
	}
	public String getUpdate_message() {
		return update_message;
	}
	public void setUpdate_message(String update_message) {
		this.update_message = update_message;
	}
	public boolean isForce_update() {
		return force_update;
	}
	public void setForce_update(boolean force_update) {
		this.force_update = force_update;
	}
	public boolean isEnable_admob_banner() {
		return enable_admob_banner;
	}
	public void setEnable_admob_banner(boolean enable_admob_banner) {
		this.enable_admob_banner = enable_admob_banner;
	}
	public boolean isEnable_admob_interstitial() {
		return enable_admob_interstitial;
	}
	public void setEnable_admob_interstitial(boolean enable_admob_interstitial) {
		this.enable_admob_interstitial = enable_admob_interstitial;
	}
	public String getAdmob_banner_key() {
		return admob_banner_key;
	}
	public void setAdmob_banner_key(String admob_banner_key) {
		this.admob_banner_key = admob_banner_key;
	}
	public String getAdmob_interstitial_key() {
		return admob_interstitial_key;
	}
	public void setAdmob_interstitial_key(String admob_interstitial_key) {
		this.admob_interstitial_key = admob_interstitial_key;
	}
	public boolean isEnable_facebook_banner() {
		return enable_facebook_banner;
	}
	public void setEnable_facebook_banner(boolean enable_facebook_banner) {
		this.enable_facebook_banner = enable_facebook_banner;
	}
	public boolean isEnable_facebook_native() {
		return enable_facebook_native;
	}
	public void setEnable_facebook_native(boolean enable_facebook_native) {
		this.enable_facebook_native = enable_facebook_native;
	}
	public boolean isEnable_facebook_interstitial() {
		return enable_facebook_interstitial;
	}
	public void setEnable_facebook_interstitial(boolean enable_facebook_interstitial) {
		this.enable_facebook_interstitial = enable_facebook_interstitial;
	}
	public String getFacebook_banner_key() {
		return facebook_banner_key;
	}
	public void setFacebook_banner_key(String facebook_banner_key) {
		this.facebook_banner_key = facebook_banner_key;
	}
	public String getFacebook_native_key() {
		return facebook_native_key;
	}
	public void setFacebook_native_key(String facebook_native_key) {
		this.facebook_native_key = facebook_native_key;
	}
	public String getFacebook_interstitial_key() {
		return facebook_interstitial_key;
	}
	public void setFacebook_interstitial_key(String facebook_interstitial_key) {
		this.facebook_interstitial_key = facebook_interstitial_key;
	}
	
	
}
