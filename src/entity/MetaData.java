package entity;

import org.json.JSONObject;

public class MetaData
{
	private int code;
	private String message;
	public MetaData() {
		super();
	}
	public MetaData(ResponseStatus status)
	{
		super();
		this.code = status.getCode();
		this.message = status.getMessage();
	}
	public MetaData(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toJSONString()
	{
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status_code", this.code);
		jsonObject.put("message", this.message);
		
		return jsonObject.toString();
	}
	public JSONObject toJSONObject()
	{
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status_code", this.code);
		jsonObject.put("message", this.message);
		
		return jsonObject;
	}
}
