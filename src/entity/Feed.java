package entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

public class Feed 
{
	private String feed_id;
	private String content;
	private List<String> images = new ArrayList<String>();
	private String link = "";
	private User user_created = new User();
	private long date_created = 0;
	private long date_modified = 0;
	private long likes_count = 0;
	private long views_count = 0;
	private List<String> likes = new ArrayList<String>();
	private List<String> views = new ArrayList<String>();
	private double lat = 0;
	private double lon = 0;
	private List<Comment> comments = new ArrayList<Comment>();
	private MetaData meta_data;
	public Document toDocument()
	{
		Document document = new Document();
		long time_interval = new Date().getTime();
		this.feed_id = "feed_" + String.valueOf(time_interval);
		document.put("feed_id", this.feed_id);
		document.put("content", this.content);
		document.put("images", this.images);
		document.put("link", this.link);
		document.put("date_created", this.date_created);
		document.put("date_modified", this.date_modified);
		document.put("likes_count", this.likes_count);
		document.put("views_count", this.views_count);
		document.put("likes", this.likes);
		document.put("views", this.views);
		Document location = new Document();
		location.put("lat", this.lat);
		location.put("lon", this.lon);
		document.put("location", location);
		document.put("comments", this.comments);
		Document user_created = new Document();
		user_created.put("user_id", this.user_created.getUser_id());
		user_created.put("full_name", this.user_created.getFull_name());
		user_created.put("avatar", this.user_created.getAvatar());
		document.put("user_created", user_created);
		return document;
	}

	public JSONObject toJSONObject()
	{
		if (meta_data.getCode() != 200)
		{
			JSONObject jsonObject = new JSONObject();
			JSONObject data = new JSONObject();
			jsonObject.put("data", data);
			JSONObject meta_data = new JSONObject();
			meta_data.put("status_code", this.meta_data.getCode());
			meta_data.put("message", this.meta_data.getMessage());
			jsonObject.put("meta_data", meta_data);
			return jsonObject;
		}
		else
		{
			JSONObject data = new JSONObject();
			JSONObject jsonObject = new JSONObject();
			data.put("feed_id", this.feed_id);
			data.put("content", this.content);
			data.put("images", this.images);
			data.put("link", this.link);
			data.put("date_created", this.date_created);
			data.put("date_modified", this.date_modified);
			data.put("likes_count", this.likes_count);
			data.put("views_count", this.views_count);
			data.put("likes", this.likes);
			data.put("views", this.views);
			JSONObject location = new JSONObject();
			location.put("lat", this.lat);
			location.put("lon", this.lon);
			data.put("location", location);
			JSONArray comments = new JSONArray();
			for (Comment comment : this.comments) 
			{
				comments.put(comment.toJSONObject());
			}
			data.put("comments", comments);
			
			JSONObject user_created = new JSONObject();
			user_created.put("user_id", this.user_created.getUser_id());
			user_created.put("full_name", this.user_created.getFull_name());
			user_created.put("avatar", this.user_created.getAvatar());
			data.put("user_created", user_created);
			jsonObject.put("data", data);
			
			JSONObject meta_data = new JSONObject();
			meta_data.put("status_code", this.meta_data.getCode());
			meta_data.put("message", this.meta_data.getMessage());
			jsonObject.put("meta_data", meta_data);
			return jsonObject;
		}
		
	}
	
	public Feed() {
		super();
	}
	public Feed(String feed_id, String content, List<String> images, String link, User user_created, long date_created,
			long date_modified, long likes_count, long views_count, double lat, double lon, List<Comment> comments) {
		super();
		this.feed_id = feed_id;
		this.content = content;
		this.images = images;
		this.link = link;
		this.user_created = user_created;
		this.date_created = date_created;
		this.date_modified = date_modified;
		this.likes_count = likes_count;
		this.views_count = views_count;
		this.lat = lat;
		this.lon = lon;
		this.comments = comments;
	}
	public String getFeed_id() {
		return feed_id;
	}
	public void setFeed_id(String feed_id) {
		this.feed_id = feed_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<String> getImages() {
		return images;
	}
	public void setImages(List<String> images) {
		this.images = images;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public User getUser_created() {
		return user_created;
	}
	public void setUser_created(User user_created) {
		this.user_created = user_created;
	}
	public long getDate_created() {
		return date_created;
	}
	public void setDate_created(long date_created) {
		this.date_created = date_created;
	}
	public long getDate_modified() {
		return date_modified;
	}
	public void setDate_modified(long date_modified) {
		this.date_modified = date_modified;
	}
	public long getLikes_count() {
		return likes_count;
	}
	public void setLikes_count(long likes_count) {
		this.likes_count = likes_count;
	}
	public long getViews_count() {
		return views_count;
	}
	public void setViews_count(long views_count) {
		this.views_count = views_count;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<String> getLikes() {
		return likes;
	}

	public void setLikes(List<String> likes) {
		this.likes = likes;
	}

	public List<String> getViews() {
		return views;
	}

	public void setViews(List<String> views) {
		this.views = views;
	}

	public MetaData getMeta_data() {
		return meta_data;
	}

	public void setMeta_data(MetaData meta_data) {
		this.meta_data = meta_data;
	}
	
	
}
