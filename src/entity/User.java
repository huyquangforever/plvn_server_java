package entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONObject;

import com.mongodb.client.model.Updates;

import utils.AppUtils;

public class User 
{
	private long user_id;
	private String email;
	private String full_name = "";
	private String password ;
	private String avatar = "";
	private String token = "";
	private String facebook_id = "";
	private String facebook_token = "";
	private String device_token = "";
	private String device_id = "";
	private long point = 0;
	private Settings user_settings;
	private MetaData meta_data;
	private double lat = 0,lon = 0;
	
	public Document toDocument()
	{
		Document user = new Document();
		long time_interval = new Date().getTime();
		this.user_id = time_interval;
		user.put("user_id", time_interval);
		user.put("email", this.email);
		user.put("full_name", this.full_name);
		user.put("avatar", this.avatar);
		user.put("password", this.password);
		user.put("point", this.point);
		user.put("facebook_id", this.facebook_id);
		
		Document settings = this.user_settings.toDocument();
		user.put("settings", settings);
		Document location = new Document();
		location.put("lat", this.lat);
		location.put("lon", this.lon);
		user.put("location", location);
		
		List<String> device_tokens = new ArrayList<>();
		device_tokens.add(this.device_token);
		user.put("device_token", device_tokens);
		
		String token = AppUtils.getToken(this.email, this.password);
		this.token = token;
		List<String> tokens = new ArrayList<>();
		tokens.add(token);
		user.put("token", tokens);
		return user;
	}
	
	public Bson toDocumentUpdate()
	{
		List<Bson> updates = new ArrayList<>();
		if (this.full_name.length() > 0)
		{
			updates.add(Updates.set("full_name", this.full_name));
		}
		if (this.avatar.length() > 0)
		{
			updates.add(Updates.set("avatar", this.avatar));
		}
		if (this.point > 0)
		{
			updates.add(Updates.inc("point", this.point));
		}
		return Updates.combine(updates);
	}
	public void parseDocument(Document document)
	{
		this.email = document.getString("email");
		this.user_id = document.getLong("user_id");
		this.full_name = document.getString("full_name");
		this.avatar = document.getString("avatar");
		this.point = document.getLong("point");
		this.facebook_id = document.getString("facebook_id");
		Document location = document.get("location", Document.class);
		this.lat = location.getDouble("lat");
		this.lon = location.getDouble("lon");
		
		Document user_settings = document.get("settings", Document.class);
		this.user_settings.parseDocument(user_settings);
	}
	
	public String toJSONString()
	{
		JSONObject jsonObject = new JSONObject();
		if (this.meta_data.getCode() == 200)
		{
			JSONObject user = new JSONObject();
			user.put("user_id", this.user_id);
			user.put("email", this.email);
			user.put("full_name", this.full_name);
			user.put("avatar", this.avatar);
			user.put("point", this.point);
			
			JSONObject settings = new JSONObject();
			settings.put("enable_law_notification", this.user_settings.isEnable_new_law_notification());
			settings.put("enable_comment_notification", this.user_settings.isEnable_new_comment_notification());
			settings.put("enable_system_notification", this.user_settings.isEnable_new_system_notification());
			settings.put("enable_nearme_notification", this.user_settings.isEnable_new_nearme_notification());
			
			user.put("settings", settings);
			
			JSONObject data = new JSONObject();
			data.put("user", user);
			data.put("token", this.token);
			
			jsonObject.put("data", data);
			jsonObject.put("meta_data", this.meta_data.toJSONObject());
		}
		else
		{
			JSONObject data = new JSONObject();
			jsonObject.put("data", data);
			jsonObject.put("meta_data", this.meta_data.toJSONObject());
		}
		return jsonObject.toString();
	}
	
	public User(User user)
	{
		super();
		this.user_id = user.user_id;
		this.email = user.email;
		this.password = user.password;
		this.full_name = user.full_name;
		this.avatar = user.avatar;
		this.token = user.token;
		this.facebook_id = user.facebook_id;
		this.facebook_token = user.facebook_token;
		this.device_token = user.device_token;
		this.device_id = user.device_id;
		this.point = user.point;
		this.user_settings = user.user_settings;
		this.meta_data = user.meta_data;
		this.lat = user.lat;
		this.lon = user.lon;
	}
	public User() 
	{
		super();
		this.meta_data = new MetaData();
		this.user_settings = new Settings();
	}
	public User(long user_id, String email, String full_name, String password, String avatar, String token,
			String facebook_id, String facebook_token, String device_token, String device_id, long point,
			Settings user_settings, MetaData meta_data, float lat, float lon) {
		super();
		this.user_id = user_id;
		this.email = email;
		this.full_name = full_name;
		this.password = password;
		this.avatar = avatar;
		this.token = token;
		this.facebook_id = facebook_id;
		this.facebook_token = facebook_token;
		this.device_token = device_token;
		this.device_id = device_id;
		this.point = point;
		this.user_settings = user_settings;
		this.meta_data = meta_data;
		this.lat = lat;
		this.lon = lon;
	}
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getFacebook_id() {
		return facebook_id;
	}
	public void setFacebook_id(String facebook_id) {
		this.facebook_id = facebook_id;
	}
	public String getFacebook_token() {
		return facebook_token;
	}
	public void setFacebook_token(String facebook_token) {
		this.facebook_token = facebook_token;
	}
	public String getDevice_token() {
		return device_token;
	}
	public void setDevice_token(String device_token) {
		this.device_token = device_token;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public long getPoint() {
		return point;
	}
	public void setPoint(long point) {
		this.point = point;
	}
	public Settings getUser_settings() {
		return user_settings;
	}
	public void setUser_settings(Settings user_settings) {
		this.user_settings = user_settings;
	}
	public MetaData getMeta_data() {
		return meta_data;
	}
	public void setMeta_data(MetaData meta_data) {
		this.meta_data = meta_data;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", email=" + email + ", full_name=" + full_name + ", password=" + password
				+ ", avatar=" + avatar + ", token=" + token + ", facebook_id=" + facebook_id + ", facebook_token="
				+ facebook_token + ", device_token=" + device_token + ", device_id=" + device_id + ", point=" + point
				+ ", user_settings=" + user_settings + ", meta_data=" + meta_data + ", lat=" + lat + ", lon=" + lon
				+ "]";
	}
	
	
}
