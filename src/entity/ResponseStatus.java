package entity;

public enum ResponseStatus 
{
	SUCCESS (200, "Success"),
	NO_DATA (201, "Không có dữ liệu"),
	REGISTER_SUCCESS (300, "Đăng kí thành công"),
	EMAIL_INVALIDATE (301, "Email không hợp lệ"),
	MISSING_FIELDS (302, "Thông tin không hợp lệ"),
	USER_ISEXIST (303, "Email đã được sử dụng trước đó"),
	LOGIN_SUCCESS (305, "Đăng nhập thành công"),
	WRONG_EMAIL_OR_PASSWORD (306, "Sai tên đăng nhập hoặc mật khẩu"),
	TOKEN_AUTHORIZED (400, "Truy vấn hợp lệ"),
	TOKEN_EXPRIED (401, "Đăng nhập đã hết hạn"),
	UNAUTHORIZED (402, "Không hợp lệ"),
	NOTEXIST (404, "Không tồn tại"),
	SOMETHING_WRONG (600, "Có lỗi xảy ra");
	private final int code;
	private final String message;
	
	private ResponseStatus(int code, String message) 
	{
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	
}
