package entity;

import java.util.Date;

import org.bson.Document;
import org.json.JSONObject;

public class Comment 
{
	private String comment_id;
	private String content = "";
	private String image ="";
	private User user_created;
	private long date_created = 0;
	private long date_modified = 0;
	private long likes_count = 0;
	
	public Document toDocument()
	{
		Document document = new Document();
		long time_interval = new Date().getTime();
		this.comment_id = "cmt_" + String.valueOf(time_interval);
		document.put("comment_id", this.comment_id);
		document.put("content", this.content);
		document.put("image", this.image);
		document.put("date_created", this.date_created);
		document.put("date_modified", this.date_modified);
		document.put("likes_count", 0);
		Document user_created = new Document();
		user_created.put("user_id", this.user_created.getUser_id());
		user_created.put("full_name", this.user_created.getFull_name());
		user_created.put("avatar", this.user_created.getAvatar());
		document.put("user_created", user_created);
		return document;
	}
	
	public JSONObject toJSONObject()
	{
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("comment_id", this.comment_id);
		jsonObject.put("content", this.content);
		jsonObject.put("image", this.image);
		jsonObject.put("date_created", this.date_created);
		jsonObject.put("date_modified", this.date_modified);
		jsonObject.put("likes_count", this.likes_count);
		JSONObject user_created = new JSONObject();
		user_created.put("user_id", this.user_created.getUser_id());
		user_created.put("full_name", this.user_created.getFull_name());
		user_created.put("avatar", this.user_created.getAvatar());
		jsonObject.put("user_created", user_created);
		return jsonObject;
	}
	
	public Comment() 
	{
		super();
		this.user_created = new User();
	}
	public Comment(String comment_id, String content, String image, User user_created, long date_created,
			long date_modified, long likes_count) {
		super();
		this.comment_id = comment_id;
		this.content = content;
		this.image = image;
		this.user_created = user_created;
		this.date_created = date_created;
		this.date_modified = date_modified;
		this.likes_count = likes_count;
	}
	public String getComment_id() {
		return comment_id;
	}
	public void setComment_id(String comment_id) {
		this.comment_id = comment_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public User getUser_created() {
		return user_created;
	}
	public void setUser_created(User user_created) {
		this.user_created = user_created;
	}
	public long getDate_created() {
		return date_created;
	}
	public void setDate_created(long date_created) {
		this.date_created = date_created;
	}
	public long getDate_modified() {
		return date_modified;
	}
	public void setDate_modified(long date_modified) {
		this.date_modified = date_modified;
	}
	public long getLikes_count() {
		return likes_count;
	}
	public void setLikes_count(long likes_count) {
		this.likes_count = likes_count;
	}
}
