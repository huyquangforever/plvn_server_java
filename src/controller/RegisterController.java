package controller;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import dao.UserDAO;
import entity.User;

public class RegisterController 
{
	public static Response registerUser(UriInfo info, HttpHeaders header)
	{
		UserDAO userDAO = new UserDAO();
		User user = new User();
		user.setEmail(info.getQueryParameters().getFirst("email"));
		user.setFull_name(info.getQueryParameters().getFirst("full_name"));
		user.setPassword(info.getQueryParameters().getFirst("password"));
		user.setAvatar(info.getQueryParameters().getFirst("avatar"));
		user.setLat(Double.parseDouble(info.getQueryParameters().getFirst("lat")));
		user.setLon(Double.parseDouble(info.getQueryParameters().getFirst("lon")));
		user.setDevice_token(info.getQueryParameters().getFirst("device_token"));
		user.setDevice_id(header.getRequestHeaders().getFirst("device_id"));
		User response = userDAO.insertNewUser(user);
		
		return Response.status(200).entity(response.toJSONString()).build();
	}
}
