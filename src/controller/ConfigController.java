package controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import dao.ConfigDAO;
import entity.Config;
import entity.MetaData;
import entity.ResponseStatus;

public class ConfigController 
{
	public static Response getConfig(UriInfo info)
	{
		String version = info.getQueryParameters().getFirst("version");
		ConfigDAO configDAO = new ConfigDAO();
		Config config = configDAO.getConfig(version);
		config.setMeta_data(new MetaData(ResponseStatus.SUCCESS));
		Response response = Response.status(200).entity(config.toJSONString()).build();
		return response;
	}
}
