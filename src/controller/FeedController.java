package controller;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.JSONObject;

import dao.FeedDAO;
import dao.UserDAO;
import entity.Feed;
import entity.ResponseStatus;
import entity.User;

public class FeedController 
{
	public static Response addNewFeed(UriInfo info, HttpHeaders header)
	{
		String token = header.getRequestHeaders().getFirst("token");
		String user_id = info.getQueryParameters().getFirst("user_id");
		User user = new User();
		user.setUser_id(Long.parseLong(user_id));
		user.setToken(token);
		if (new UserDAO().authorizedUser(user).getCode() == ResponseStatus.TOKEN_AUTHORIZED.getCode())
		{
			 String content = info.getQueryParameters().getFirst("content");
			 List<String> images = info.getQueryParameters().get("images");
			 String link = info.getQueryParameters().getFirst("link");
			 long date_created = Long.parseLong(info.getQueryParameters().getFirst("date_created"));
			 double lat = Double.parseDouble(info.getQueryParameters().getFirst("lat"));
			 double lon = Double.parseDouble(info.getQueryParameters().getFirst("lon"));
			 String full_name = info.getQueryParameters().getFirst("full_name");
			 String avatar = info.getQueryParameters().getFirst("avatar");
			 user.setFull_name(full_name);
			 user.setAvatar(avatar);
			 Feed feed = new Feed();
			 feed.setContent(content);
			 feed.setImages(images);
			 feed.setLink(link);
			 feed.setDate_created(date_created);
			 feed.setDate_modified(date_created);
			 feed.setLat(lat);
			 feed.setLon(lon);
			 feed.setUser_created(user);
			 
			 Feed response = new FeedDAO().insertNewFeed(feed);
			 return Response.status(200).entity(response.toJSONObject().toString()).build();
		}
		else
		{
			JSONObject response = new JSONObject();
			response.put("data", new JSONObject());
			JSONObject meta_data = new JSONObject();
			meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
			meta_data.put("message", "Truy cập không hợp lệ");
			response.put("meta_data", meta_data);
			return Response.status(200).entity(response.toString()).build();
		}
	}
}
