package controller;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.JSONObject;

import dao.UserDAO;
import entity.ResponseStatus;
import entity.User;
import helper.HTTPSHelper;

public class LoginController {
	public static Response userLogin(UriInfo info)
	{
		String email = info.getQueryParameters().getFirst("email");
		String password = info.getQueryParameters().getFirst("password");
		String device_token = info.getQueryParameters().getFirst("device_token");
		UserDAO userDAO = new UserDAO();
		User user = userDAO.getUserWith(email, password, device_token);
		
		return Response.status(200).entity(user.toJSONString()).build();
	}
	
	public static Response facebookLogin(UriInfo info)
	{
		String email = info.getQueryParameters().getFirst("email");
		String full_name = info.getQueryParameters().getFirst("full_name");
		String avatar = info.getQueryParameters().getFirst("avatar");
		String facebook_id = info.getQueryParameters().getFirst("facebook_id");
		String device_token = info.getQueryParameters().getFirst("device_token");
		String facebook_token = info.getQueryParameters().getFirst("facebook_token");
		JSONObject response_access_token = HTTPSHelper.GETRequest("https://graph.facebook.com/app?access_token="+facebook_token);
		if (!response_access_token.isNull("id"))
		{
			if (response_access_token.getString("id").equals("238970583184585"))
			{
				JSONObject response_facebook = HTTPSHelper.GETRequest("https://graph.facebook.com/me?fields=id,name,picture.type(large).width(720),email&access_token="+facebook_token);
				if (!response_facebook.isNull("id"))
				{
					if (response_facebook.getString("id").equals(facebook_id))
					{
						User user_request = new User();
						user_request.setEmail(email);
						user_request.setFull_name(full_name);
						user_request.setAvatar(avatar);
						user_request.setFacebook_id(facebook_id);
						user_request.setDevice_token(device_token);
						User user_response = new UserDAO().insertOrUpdateNewUser(user_request);
						return Response.status(200).entity(user_response.toJSONString()).build();
					}
					else
					{
						JSONObject response = new JSONObject();
						response.put("data", new JSONObject());
						JSONObject meta_data = new JSONObject();
						meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
						meta_data.put("message", ResponseStatus.UNAUTHORIZED.getMessage());
						response.put("meta_data", meta_data);
						return Response.status(200).entity(response.toString()).build();
					}
				}
				else
				{
					JSONObject response = new JSONObject();
					response.put("data", new JSONObject());
					JSONObject meta_data = new JSONObject();
					meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
					meta_data.put("message", ResponseStatus.UNAUTHORIZED.getMessage());
					response.put("meta_data", meta_data);
					return Response.status(200).entity(response.toString()).build();
				}
			}
			else
			{
				JSONObject response = new JSONObject();
				response.put("data", new JSONObject());
				JSONObject meta_data = new JSONObject();
				meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
				meta_data.put("message", ResponseStatus.UNAUTHORIZED.getMessage());
				response.put("meta_data", meta_data);
				return Response.status(200).entity(response.toString()).build();
			}
			
		}
		else
		{
			JSONObject response = new JSONObject();
			response.put("data", new JSONObject());
			JSONObject meta_data = new JSONObject();
			meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
			meta_data.put("message", ResponseStatus.UNAUTHORIZED.getMessage());
			response.put("meta_data", meta_data);
			return Response.status(200).entity(response.toString()).build();
		}
		
		
	}
	
	public static Response userLogout(UriInfo info, HttpHeaders header)
	{
		String user_id = info.getQueryParameters().getFirst("user_id");
		String token = header.getRequestHeaders().getFirst("token");
		new UserDAO().userLogout(user_id, token);
		JSONObject response = new JSONObject();
		response.put("data", new JSONObject());
		JSONObject meta_data = new JSONObject();
		meta_data.put("status_code", ResponseStatus.SUCCESS.getCode());
		meta_data.put("message", ResponseStatus.SUCCESS.getMessage());
		response.put("meta_data", meta_data);
		return Response.status(200).entity(response.toString()).build();
	}
}
