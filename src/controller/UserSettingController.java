package controller;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.JSONObject;

import dao.UserDAO;
import entity.ResponseStatus;
import entity.User;

public class UserSettingController 
{
	public static Response updateUserInfo(UriInfo info, HttpHeaders header)
	{
		User user = new User();
		String token = header.getRequestHeaders().getFirst("token");
		String user_id = info.getPathParameters().getFirst("user_id");
		System.out.println(token);
		System.out.println(user_id);
		UserDAO userDAO = new UserDAO();
		user.setUser_id(Long.parseLong(user_id));
		user.setToken(token);
		if (userDAO.authorizedUser(user).getCode() == ResponseStatus.TOKEN_AUTHORIZED.getCode())
		{
			if (info.getQueryParameters().containsKey("full_name"))
			{
				user.setFull_name(info.getQueryParameters().getFirst("full_name"));
			}
			if (info.getQueryParameters().containsKey("avatar"))
			{
				user.setFull_name(info.getQueryParameters().getFirst("avatar"));
			}
			if (info.getQueryParameters().containsKey("point"))
			{
				user.setPoint(Long.parseLong(info.getQueryParameters().getFirst("point")));
			}
			User response = userDAO.updateUserInfo(user);
			return Response.status(200).entity(response.toJSONString()).build();
		}else
		{
			JSONObject response = new JSONObject();
			response.put("data", new JSONObject());
			JSONObject meta_data = new JSONObject();
			meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
			meta_data.put("message", "Truy cập không hợp lệ");
			response.put("meta_data", meta_data);
			return Response.status(200).entity(response.toString()).build();
		}
	}
	
	public static Response changePassword(UriInfo info, HttpHeaders header)
	{
		String user_id = info.getPathParameters().getFirst("user_id");
		String token = header.getRequestHeaders().getFirst("token");
		String oldPassword = info.getQueryParameters().getFirst("old_password");
		String password = info.getQueryParameters().getFirst("password");
		UserDAO userDAO = new UserDAO();
		User user = new User();
		user.setUser_id(Long.parseLong(user_id));
		user.setToken(token);
		if (userDAO.authorizedUser(user).getCode() == ResponseStatus.TOKEN_AUTHORIZED.getCode())
		{
			if (userDAO.checkPassword(user_id, oldPassword))
			{
				ResponseStatus responseStatus = userDAO.changePassword(user_id, password);
				JSONObject response = new JSONObject();
				response.put("data", new JSONObject());
				JSONObject meta_data = new JSONObject();
				meta_data.put("status_code", responseStatus.getCode());
				meta_data.put("message", responseStatus.getMessage());
				response.put("meta_data", meta_data);
				return Response.status(200).entity(response.toString()).build();
			}
			else
			{
				JSONObject response = new JSONObject();
				response.put("data", new JSONObject());
				JSONObject meta_data = new JSONObject();
				meta_data.put("status_code", ResponseStatus.WRONG_EMAIL_OR_PASSWORD.getCode());
				meta_data.put("message", "Mật khẩu cũ không đúng");
				response.put("meta_data", meta_data);
				return Response.status(200).entity(response.toString()).build();
			}
		}
		else
		{
			JSONObject response = new JSONObject();
			response.put("data", new JSONObject());
			JSONObject meta_data = new JSONObject();
			meta_data.put("status_code", ResponseStatus.UNAUTHORIZED.getCode());
			meta_data.put("message", "Truy cập không hợp lệ");
			response.put("meta_data", meta_data);
			return Response.status(200).entity(response.toString()).build();
		}
	}
}
